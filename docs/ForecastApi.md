# WeatherApi.ForecastApi

All URIs are relative to *http://api.weather.chrisbaptista.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAlerts**](ForecastApi.md#getAlerts) | **GET** /forecast/{lat},{lng}/alerts | Get Weather Alerts
[**getCurrentWeather**](ForecastApi.md#getCurrentWeather) | **GET** /forecast/{lat},{lng}/current | Get Current Weather
[**getDailyForecast**](ForecastApi.md#getDailyForecast) | **GET** /forecast/{lat},{lng}/daily | Get Daily Forecast
[**getForecast**](ForecastApi.md#getForecast) | **GET** /forecast/{lat},{lng} | Get Forecast
[**getHourlyForecast**](ForecastApi.md#getHourlyForecast) | **GET** /forecast/{lat},{lng}/hourly | Get Hourly Forecast
[**getMinutelyForecast**](ForecastApi.md#getMinutelyForecast) | **GET** /forecast/{lat},{lng}/minutely | Get Minutely Forecast


<a name="getAlerts"></a>
# **getAlerts**
> Object getAlerts(lat, lng)

Get Weather Alerts

Get Weather Alerts for Geo-Coordinates, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ForecastApi();

var lat = "lat_example"; // String | Latitude

var lng = "lng_example"; // String | Longitude


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAlerts(lat, lng, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **String**| Latitude | 
 **lng** | **String**| Longitude | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCurrentWeather"></a>
# **getCurrentWeather**
> Object getCurrentWeather(lat, lng)

Get Current Weather

Get Current Weather for Geo-Coordinates, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ForecastApi();

var lat = "lat_example"; // String | Latitude

var lng = "lng_example"; // String | Longitude


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCurrentWeather(lat, lng, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **String**| Latitude | 
 **lng** | **String**| Longitude | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDailyForecast"></a>
# **getDailyForecast**
> Object getDailyForecast(lat, lng)

Get Daily Forecast

Get Daily Forecast for Geo-Coordinates, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ForecastApi();

var lat = "lat_example"; // String | Latitude

var lng = "lng_example"; // String | Longitude


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getDailyForecast(lat, lng, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **String**| Latitude | 
 **lng** | **String**| Longitude | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getForecast"></a>
# **getForecast**
> Object getForecast(lat, lng)

Get Forecast

Get all weather data for Geo-Coordinates, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ForecastApi();

var lat = "lat_example"; // String | Latitude

var lng = "lng_example"; // String | Longitude


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getForecast(lat, lng, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **String**| Latitude | 
 **lng** | **String**| Longitude | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getHourlyForecast"></a>
# **getHourlyForecast**
> Object getHourlyForecast(lat, lng)

Get Hourly Forecast

Get Hourly Forecast for Geo-Coordinates, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ForecastApi();

var lat = "lat_example"; // String | Latitude

var lng = "lng_example"; // String | Longitude


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getHourlyForecast(lat, lng, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **String**| Latitude | 
 **lng** | **String**| Longitude | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMinutelyForecast"></a>
# **getMinutelyForecast**
> Object getMinutelyForecast(lat, lng)

Get Minutely Forecast

Get Minutely Forecast for Geo-Coordinates, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ForecastApi();

var lat = "lat_example"; // String | Latitude

var lng = "lng_example"; // String | Longitude


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getMinutelyForecast(lat, lng, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **String**| Latitude | 
 **lng** | **String**| Longitude | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

