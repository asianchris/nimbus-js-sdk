# WeatherApi.ZipcodeApi

All URIs are relative to *http://api.weather.chrisbaptista.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**findZipcode**](ZipcodeApi.md#findZipcode) | **GET** /zipcode/search | Find Zip Code
[**getZipcode**](ZipcodeApi.md#getZipcode) | **GET** /zipcode/{zip} | Get Zip Code


<a name="findZipcode"></a>
# **findZipcode**
> Object findZipcode(city, state)

Find Zip Code

Find Zip Code by City and State, data provided by Zipcode API

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ZipcodeApi();

var city = "city_example"; // String | City

var state = "state_example"; // String | State


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.findZipcode(city, state, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **city** | **String**| City | 
 **state** | **String**| State | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getZipcode"></a>
# **getZipcode**
> Object getZipcode(zip)

Get Zip Code

Get Zip Code, data provided by Zipcode API

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ZipcodeApi();

var zip = "zip_example"; // String | Zipcode


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getZipcode(zip, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zip** | **String**| Zipcode | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

