# WeatherApi.ZipcodeForecastApi

All URIs are relative to *http://api.weather.chrisbaptista.io/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAlerts**](ZipcodeForecastApi.md#getAlerts) | **GET** /zipcode/{zip}/weather/alerts | Get Weather Alerts for Zip Code
[**getCurrentWeather**](ZipcodeForecastApi.md#getCurrentWeather) | **GET** /zipcode/{zip}/weather | Get Current Weather for Zip Code
[**getDailyForecast**](ZipcodeForecastApi.md#getDailyForecast) | **GET** /zipcode/{zip}/forecast/daily | Get Daily Forecast for Zip Code
[**getForecast**](ZipcodeForecastApi.md#getForecast) | **GET** /zipcode/{zip}/forecast | Get Forecast for Zip Code
[**getHourlyForecast**](ZipcodeForecastApi.md#getHourlyForecast) | **GET** /zipcode/{zip}/forecast/hourly | Get Hourly Forecast for Zip Code
[**getMinutelyForecast**](ZipcodeForecastApi.md#getMinutelyForecast) | **GET** /zipcode/{zip}/forecast/minutely | Get Minutely Forecast for Zip Code


<a name="getAlerts"></a>
# **getAlerts**
> Object getAlerts(zip)

Get Weather Alerts for Zip Code

Get Weather Alerts for Zip Code, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ZipcodeForecastApi();

var zip = "zip_example"; // String | Zipcode


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAlerts(zip, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zip** | **String**| Zipcode | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCurrentWeather"></a>
# **getCurrentWeather**
> Object getCurrentWeather(zip)

Get Current Weather for Zip Code

Get Current Weather for Zip Code, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ZipcodeForecastApi();

var zip = "zip_example"; // String | Zipcode


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCurrentWeather(zip, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zip** | **String**| Zipcode | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDailyForecast"></a>
# **getDailyForecast**
> Object getDailyForecast(zip)

Get Daily Forecast for Zip Code

Get Daily Forecast for Zip Code, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ZipcodeForecastApi();

var zip = "zip_example"; // String | Zipcode


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getDailyForecast(zip, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zip** | **String**| Zipcode | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getForecast"></a>
# **getForecast**
> Object getForecast(zip)

Get Forecast for Zip Code

Get all weather data for Zip Code, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ZipcodeForecastApi();

var zip = "zip_example"; // String | Zipcode


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getForecast(zip, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zip** | **String**| Zipcode | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getHourlyForecast"></a>
# **getHourlyForecast**
> Object getHourlyForecast(zip)

Get Hourly Forecast for Zip Code

Get Hourly Forecast for Zip Code, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ZipcodeForecastApi();

var zip = "zip_example"; // String | Zipcode


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getHourlyForecast(zip, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zip** | **String**| Zipcode | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMinutelyForecast"></a>
# **getMinutelyForecast**
> Object getMinutelyForecast(zip)

Get Minutely Forecast for Zip Code

Get Minutely Forecast for Zip Code, data provided by forecast.io

### Example
```javascript
var WeatherApi = require('weather_api');
var defaultClient = WeatherApi.ApiClient.default;

// Configure API key authorization: apiKey
var apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

var apiInstance = new WeatherApi.ZipcodeForecastApi();

var zip = "zip_example"; // String | Zipcode


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getMinutelyForecast(zip, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zip** | **String**| Zipcode | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

